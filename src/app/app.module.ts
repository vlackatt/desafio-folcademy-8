import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioModule } from './componentes/inicio/inicio.module';
import { RoutesModule } from './componentes/routes/routes.module';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RoutesModule,
    InicioModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
